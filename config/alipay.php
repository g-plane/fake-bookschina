<?php

return [
    'app_id' => env('ALIPAY_APP_ID', '2016101200667943'),

    'mode' => env('ALIPAY_MODE', 'dev'),

    'ali_public_key' => env(
        'ALIPAY_PAY_PUBLIC_KEY',
        storage_path('keys/pay_public_key.example.pem')
    ),

    'private_key' => env(
        'ALIPAY_APP_PRIVATE_KEY',
        storage_path('keys/app_private_key.example.pem')
    ),

    'notify_url' => env('APP_URL').'/pay/notify',

    'return_url' => env('APP_URL').'/pay/return',
];
