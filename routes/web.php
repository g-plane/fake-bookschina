<?php

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'auth'], function () {
    Route::view('login', 'auth.login')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::view('reg', 'auth.reg');
    Route::post('reg', 'Auth\RegisterController@register');
    Route::view('forget', 'auth.forget');
    Route::post('forget', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('reset', 'Auth\ResetPasswordController@reset');
    Route::any('captcha', 'AuthController@captcha');
});

Route::group([], function () {
    Route::get('search', 'BookController@list');

    Route::group(['prefix' => 'book'], function () {
        Route::get('{id}', 'BookController@detail');
        Route::post('{id}/comment', 'BookController@comment')->middleware('auth');
    });
});

Route::group(['prefix' => 'shoppingCart', 'middleware' => 'auth'], function () {
    Route::view('', 'shoppingCart.list');
    Route::get('list', 'ShoppingCartController@list');
    Route::post('add', 'ShoppingCartController@add');
    Route::post('remove', 'ShoppingCartController@remove');
    Route::post('decrement', 'ShoppingCartController@decrement');
    Route::post('set', 'ShoppingCartController@setAmount');
});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
    Route::redirect('', 'user/orders');

    Route::group(['prefix' => 'orders'], function () {
        Route::get('', 'OrdersController@index');
        Route::get('{id}', 'OrdersController@detail');
        Route::post('cancel', 'OrdersController@cancel');
    });

    Route::group(['prefix' => 'favorites'], function () {
        Route::view('', 'user.favorites');
        Route::get('list', 'FavoriteController@list');
        Route::post('add', 'FavoriteController@add');
        Route::post('remove', 'FavoriteController@remove');
    });

    Route::group(['prefix' => 'address'], function () {
        Route::view('', 'user.address');
        Route::get('list', 'AddressController@list');
        Route::post('add', 'AddressController@add');
        Route::post('update', 'AddressController@update');
        Route::post('remove', 'AddressController@remove');
    });

    Route::group(['prefix' => 'account'], function () {
        Route::view('', 'user.account');
        Route::post('password', 'AccountController@password');
        Route::post('delete', 'AccountController@delete');
    });
});

Route::group(['prefix' => 'pay'], function () {
    Route::post('notify', 'PaymentController@notify'); // DO NOT add `auth` middleware.

    Route::group(['middleware' => 'auth'], function () {
        Route::any('prepare', 'PaymentController@prepare');
        Route::post('send', 'PaymentController@send');
        Route::get('return', 'PaymentController@return');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::view('', 'admin.base');

    Route::group(['prefix' => 'users'], function () {
        Route::get('list', 'UsersManagementController@list');
        Route::post('update', 'UsersManagementController@update');
        Route::post('remove', 'UsersManagementController@remove');
    });

    Route::group(['prefix' => 'books'], function () {
        Route::get('list', 'BooksManagementController@list');
        Route::get('info', 'BooksManagementController@info');
        Route::post('add', 'BooksManagementController@add');
        Route::post('update', 'BooksManagementController@update');
        Route::post('cover', 'BooksManagementController@cover');
        Route::post('remove', 'BooksManagementController@remove');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('list', 'CategoriesManagementController@list');
        Route::get('tree', 'CategoriesManagementController@tree');
        Route::get('all-trees', 'CategoriesManagementController@allTrees');
        Route::get('ancestors', 'CategoriesManagementController@ancestors');
        Route::post('add', 'CategoriesManagementController@add');
        Route::post('update', 'CategoriesManagementController@update');
        Route::post('remove', 'CategoriesManagementController@remove');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('list', 'OrdersManagementController@list');
        Route::get('books', 'OrdersManagementController@books');
        Route::post('update', 'OrdersManagementController@update');
        Route::post('cancel', 'OrdersManagementController@cancel');
    });

    Route::group(['prefix' => 'comments'], function () {
        Route::get('list', 'CommentsManagementController@list');
        Route::post('content', 'CommentsManagementController@content');
        Route::post('rating', 'CommentsManagementController@rating');
        Route::post('delete', 'CommentsManagementController@delete');
    });
});

Route::group(['prefix' => 'faq'], function () {
    Route::group(['prefix' => 'Buy'], function () {
        Route::view('peison', 'faq.buy.peison');
    });

    Route::group(['prefix' => 'otherInfo'], function () {
        Route::view('about', 'faq.otherInfo.about');
    });

    Route::group(['prefix' => 'toNews'], function () {
        Route::view('price', 'faq.toNews.price');
        Route::view('question', 'faq.toNews.question');
        Route::view('buy','faq.toNews.buy');
    });
});

Route::group(['prefix' => 'subjects'], function () {
    Route::view('bestSelling','subjects.bestSelling');
});
