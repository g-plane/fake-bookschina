<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookOrderTable extends Migration
{
    public function up()
    {
        Schema::create('book_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id');
            $table->bigInteger('book_id');
            $table->integer('amount');
            $table->bigInteger('user_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('book_order');
    }
}
