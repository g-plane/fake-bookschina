<?php

use App\Models\Book;
use App\Models\User;
use Faker\Generator;
use App\Models\Comment;

$factory->define(Comment::class, function (Generator $faker) {
    $zhCNGenerator = Faker\Factory::create('zh_CN');

    return [
        'user_id' => User::select('id')->inRandomOrder()->first()->id,
        'book_id' => Book::select('id')->inRandomOrder()->first()->id,
        'content' => $faker->paragraph,
        'rating' => 'good',
    ];
});
