<?php

use App\Models\Book;
use Faker\Generator;
use App\Models\Category;

$hasCategories = Category::count() > 0;

$factory->define(Book::class, function (Generator $faker) use ($hasCategories) {
    $zhCNGenerator = Faker\Factory::create('zh_CN');

    return [
        'name' => $faker->word,
        'price' => round($faker->randomFloat(2, 5, 200), 1),
        'amount' => $faker->randomNumber(3),
        'author' => $zhCNGenerator->name,
        'isbn' => $faker->isbn13,
        'publisher' => $zhCNGenerator->company,
        'publish_time' => $faker->dateTimeBetween($startDate = '-10 years'),
        'category_id' => $hasCategories ? Category::inRandomOrder()->first()->id : 0,
        'img' => $faker->imageUrl(480, 640, $faker->randomElement(['food', 'cats'])),
        'description' => $faker->paragraph,
        'detail' => $faker->text(400),
    ];
});
