const mix = require('laravel-mix')

mix.js('resources/js/auth/captcha.js', 'public/js/captcha.js')
mix.js('resources/js/toNews/sidebar.js', 'public/js/toNews/sidebar.js')
mix.js('resources/js/shoppingCart/list.js', 'public/js/shoppingCart.js')
mix.js('resources/js/pay/prepare.js', 'public/js/prepare-pay.js')
mix.js('resources/js/admin/app.js', 'public/js/admin.js')
mix.js('resources/js/book/list.js', 'public/js/books-list.js')
mix.js('resources/js/book/detail.js', 'public/js/book-detail.js')
mix.js('resources/js/user/index.js', 'public/js/user.js')
mix.js('resources/js/user/favorites.js', 'public/js/fav.js')
mix.js('resources/js/user/address.js', 'public/js/address.js')
mix.js('resources/js/index/index.js', 'public/js/index.js')

mix.stylus('resources/stylus/index.styl', 'public/css/index.css')
mix.stylus('resources/stylus/auth.styl', 'public/css/auth.css')
mix.stylus('resources/stylus/toNews.styl', 'public/css/toNews.css')
mix.stylus('resources/stylus/rightNav.styl','public/css/rightNav.css')
mix.stylus('resources/stylus/list.styl', 'public/css/list.css')
mix.stylus('resources/stylus/detail.styl', 'public/css/detail.css')
mix.stylus('resources/stylus/pay.styl', 'public/css/pay.css')
mix.stylus('resources/stylus/user.styl', 'public/css/user.css')
mix.stylus('resources/stylus/user-account.styl', 'public/css/user-account.css')
mix.stylus('resources/stylus/user-orders.styl', 'public/css/user-orders.css')

mix.copy('node_modules/sweetalert2/dist/sweetalert2.min.css', 'public/vendor/sweetalert2.min.css')

mix.copy('resources/images/logo.png', 'public/images/logo.png')
mix.copy('resources/images/bottomBar.jpg', 'public/images/bottomBar.jpg')
mix.copy('resources/images/cart.png', 'public/images/cart.png')
mix.copy('resources/images/favorite.png', 'public/images/favorite.png')
mix.copy('resources/images/arrow.png', 'public/images/arrow.png')
mix.copy('resources/images/upArrow.png', 'public/images/upArrow.png')
mix.copy('resources/images/user.png', 'public/images/user.png')
mix.copy('resources/images/topBanner.png', 'public/images/topBanner.png')

mix.version()
