<?php

namespace App\Console\Commands;

use Hash;
use App\Models\User;
use Illuminate\Console\Command;

class AdminCreateCommand extends Command
{
    protected $signature = 'admin:create {email} {password} {username}';

    protected $description = 'Create a new administrator.';

    public function handle()
    {
        $user = new User();
        $user->email = $this->argument('email');
        $user->password = Hash::make($this->argument('password'));
        $user->username = $this->argument('username');
        $user->is_admin = true;
        $user->save();

        $this->info('Administrator created successfully.');
    }
}
