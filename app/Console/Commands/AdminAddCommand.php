<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class AdminAddCommand extends Command
{
    protected $signature = 'admin:add {user_id}';

    protected $description = 'Mark an existing user as an administrator.';

    public function handle()
    {
        $user = User::findOrFail($this->argument('user_id'));
        $user->is_admin = true;
        $user->save();

        $this->info('User '.$user->email.' is an administrator now.');
    }
}
