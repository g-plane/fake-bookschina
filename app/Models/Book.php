<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $casts = [
        'price' => 'float',
        'amount' => 'integer',
        'publish_time' => 'date:Y-m-d',
    ];

    protected $fillable = [
        'name', 'price', 'amount', 'author', 'isbn',
        'publisher', 'publish_time', 'category_id',
        'img', 'description', 'detail',
    ];

    public function getPublishDateAttribute()
    {
        return $this->publish_time->toDateString();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
