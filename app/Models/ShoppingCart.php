<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $casts = [
        'user_id' => 'integer',
        'book_id' => 'integer',
        'amount' => 'integer',
    ];

    protected $fillable = [
        'user_id', 'book_id', 'amount',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
