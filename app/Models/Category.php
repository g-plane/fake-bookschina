<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
    ];

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }

    public function ancestors()
    {
        $ancestors = collect([$this]);
        while (true) {
            $end = $ancestors->last();
            $parent = $end->parent;
            if ($parent) {
                $ancestors->push($parent);
            } else {
                break;
            }
        }

        return $ancestors->map(function ($ancestor) {
            return ['id' => $ancestor->id, 'name' => $ancestor->name];
        });
    }

    public function childrenTree($depth = 3)
    {
        return Collection::times($depth)
            ->reduce(function ($all, $current) {
                $result = $all[$current - 1]->map(function ($item) {
                    return $item->children;
                })->flatten();
                $all->push($result);

                return $all;
            }, collect([collect([$this])]));
    }
}
