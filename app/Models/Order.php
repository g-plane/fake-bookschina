<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'paid_at' => 'datetime',
        'price' => 'float',
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class)
            ->withPivot('amount', 'user_id');
    }
}
