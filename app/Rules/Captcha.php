<?php

namespace App\Rules;

use Gregwar\Captcha\CaptchaBuilder;
use Illuminate\Contracts\Validation\Rule;

class Captcha implements Rule
{
    public function passes($attribute, $value)
    {
        $builder = new CaptchaBuilder(session()->pull('captcha'));
        return $builder->testPhrase($value);
    }

    public function message()
    {
        return '验证码不正确';
    }
}
