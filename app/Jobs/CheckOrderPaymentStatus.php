<?php

namespace App\Jobs;

use Exception;
use App\Models\Order;
use Yansongda\Pay\Pay;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckOrderPaymentStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;

    protected $order;

    protected $config;

    public function __construct(Order $order, array $config)
    {
        $this->order = $order;
        $this->config = $config;
    }

    public function handle()
    {
        if ($this->order->status !== 'paying') {
            return;
        }

        $payment = Pay::alipay($this->config);
        $response = $payment->find($this->order->id);
        if (
            $response['trade_status'] === 'TRADE_SUCCESS' ||
            $response['trade_status'] === 'TRADE_FINISHED'
        ) {
            $this->order->paid_at = $response['send_pay_date'];
            $this->order->status = 'paid';
            $this->order->save();
        }
    }
}
