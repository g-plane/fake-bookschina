<?php

namespace App\Http\Controllers;

use Hash;
use App\Models\User;
use App\Models\ShoppingCart;
use Illuminate\Http\Request;

class UsersManagementController extends Controller
{
    public function list(Request $request)
    {
        $perPage = $request->input('per-page', 10);

        return User::paginate($perPage);
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'username' => 'required|string',
            'email' => 'email|nullable|unique:users',
            'password' => 'string|nullable|min:8',
        ]);

        $user = User::find($data['id']);
        if (empty($user)) {
            return response()->json(['code' => 1, 'message' => '用户不存在']);
        }

        $user->username = $data['username'];
        if (! empty($data['email'])) {
            $user->email = $data['email'];
        }
        if (! empty($data['password']) && ! $user->is_admin) {
            $user->password = Hash::make($data['password']);
        }

        $user->save();

        return response()->json(['code' => 0]);
    }

    public function remove(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $user = User::find($id);
        if (empty($user)) {
            return response()->json(['code' => 1, 'message' => '用户不存在']);
        }

        if ($user->is_admin) {
            return response()->json(['code' => 1, 'message' => '管理员不能被删除']);
        }

        ShoppingCart::where('user_id', $id)->delete();
        $user->favorites()->delete();
        $user->delete();

        return response()->json(['code' => 0]);
    }
}
