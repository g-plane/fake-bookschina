<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesManagementController extends Controller
{
    public function list()
    {
        return Category::all();
    }

    public function add(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'parent' => 'integer',
        ]);

        if ($data['parent'] != 0 && Category::where('id', $data['parent'])->doesntExist()) {
            return response()->json(['code' => 1, 'message' => '父分类不存在']);
        }

        if (Category::where('name', $data['name'])->exists()) {
            return response()->json(['code' => 1, 'message' => '分类名称已被使用']);
        }

        $category = new Category();
        $category->name = $data['name'];
        $category->parent_id = $data['parent'];
        $category->save();

        return response()->json(['code' => 0, 'category' => $category]);
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'name' => 'required|string',
            'parent' => 'integer',
        ]);

        if ($data['parent'] != 0 && ! Category::find($data['parent'])) {
            return response()->json(['code' => 1, 'message' => '父分类不存在']);
        }

        $category = Category::findOrFail($data['id']);
        if (
            $category->name !== $data['name'] &&
            Category::where('name', $data['name'])->exists()
        ) {
            return response()->json(['code' => 1, 'message' => '分类名称已被使用']);
        }

        $category->name = $data['name'];
        $category->parent_id = $data['parent'];
        $category->save();

        return response()->json(['code' => 0]);
    }

    public function remove(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $category = Category::findOrFail($data['id']);
        if ($category->children->isNotEmpty()) {
            return response()->json([
                'code' => 1,
                'message' => '当前分类下有子分类，不能直接删除。',
            ]);
        }

        if ($category->books->isNotEmpty()) {
            return response()->json([
                'code' => 1,
                'message' => '当前分类下有图书。请先删除图书或将它们转移到其它分类。',
            ]);
        }

        $category->delete();
        return response()->json(['code' => 0]);
    }

    public function tree(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $root = Category::find($data['id']);
        if (! $root) {
            return collect([]);
        }

        return $root->childrenTree()[0];
    }

    public function allTrees()
    {
        return Category::where('parent_id', 0)
            ->get()
            ->map(function ($category) {
                return $category->childrenTree()[0][0];
            });
    }

    public function ancestors(Request $request)
    {
        $start = Category::find($request->input('id'));

        return empty($start) ? collect() : $start->ancestors();
    }
}
