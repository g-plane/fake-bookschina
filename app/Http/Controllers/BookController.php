<?php

namespace App\Http\Controllers;

use DB;
use Cache;
use App\Models\Book;
use App\Models\Comment;
use App\Models\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function list(Request $request, Book $books)
    {
        $keyword = $request->input('q');
        if ($keyword) {
            $keywords = explode(' ', $keyword);
            $books = $books->where(function ($query) use ($keywords) {
                foreach ($keywords as $keyword) {
                    $query->orWhere('name', 'LIKE', "%$keyword%");
                }
            });
        }

        $author = $request->input('author');
        if ($author) {
            $books = $books->where('author', $author);
        }

        $publisher = $request->input('publisher');
        if ($publisher) {
            $books = $books->where('publisher', $publisher);
        }

        $categoryId = $request->input('category');
        if ($categoryId) {
            $category = Category::find($categoryId);
            if ($category) {
                $categories = $category
                    ->childrenTree()
                    ->flatten()
                    ->unique()
                    ->pluck('id')
                    ->all();
            } else {
                $categories = [];
            }

            $books = $books->whereIn('category_id', $categories);
        }

        $books = $books->paginate(10);
        $user = $request->user();

        return view('book.list', [
            'books' => $books,
            'user' => $user,
            'favorites' => $user ? $user->favorites : collect(),
        ]);
    }

    public function detail($id)
    {
        $book = Cache::rememberForever('book_model-'.$id, function () use ($id) {
            return Book::findOrFail($id);
        });
        $category = $book->category;
        if (empty($category)) {
            $categories = collect();
        } else {
            $categories = $category->ancestors()->reverse()->values();
        }

        $user = auth()->user();
        if ($user) {
            $favored = $user->favorites->contains($id);
        } else {
            $favored = false;
        }

        $ratingLabels = [
            'bad' => '差评',
            'medium' => '中评',
            'good' => '好评',
        ];
        $comments = $book
            ->comments()
            ->paginate(10)
            ->setPageName('comment-page');

        $canComment = auth()->check() &&
            DB::table('book_order')
                ->where('book_id', $book->id)
                ->where('user_id', $user->id)
                ->exists() &&
            ! $comments->contains(function ($comment) use ($user) {
                return $comment->user_id === $user->id;
            });

        return view('book.detail', [
            'book' => $book,
            'categories' => $categories,
            'ratingLabels' => $ratingLabels,
            'comments' => $comments,
            'canComment' => $canComment,
            'favored' => $favored,
            'youMayLike' => $this->guessYouMayLike(),
        ]);
    }

    public function guessYouMayLike($amount = 5)
    {
        return Book::inRandomOrder()->take($amount)->get();
    }

    public function comment(Request $request, $id)
    {
        $data = $this->validate($request, [
            'content' => 'required|string',
            'rating' => 'required|in:bad,medium,good',
        ]);

        $comment = new Comment();
        $comment->user_id = auth()->id();
        $comment->book_id = $id;
        $comment->content = $data['content'];
        $comment->rating = $data['rating'];
        $comment->save();

        return back()->with('open-comments', true);
    }
}
