<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Order;
use Yansongda\Pay\Pay;
use Illuminate\Http\Request;
use App\Jobs\CheckOrderPaymentStatus;

class PaymentController extends Controller
{
    public function prepare(Request $request)
    {
        $session = $request->session();
        if ($request->has('books')) {
            $ids = array_keys($request->input('books'));
            $session->put('purchasing_candidates', $ids);
        } else {
            $ids = $session->get('purchasing_candidates');
        }

        $user = $request->user();
        $shoppingCart = $user->shoppingCart;
        $list = collect($ids)
            ->filter(function ($id) use ($shoppingCart) {
                return $shoppingCart->contains($id);
            })
            ->map(function ($id) use ($shoppingCart) {
                return $shoppingCart->firstWhere('id', $id);
            });
        $total = $list->sum(function ($cartItem) {
            return $cartItem->book->price * $cartItem->amount;
        });
        $hasShippingFee = $total < 69;
        if ($hasShippingFee) {
            $total += 8;
        }

        return view('pay.prepare', [
            'list' => $list,
            'total' => $total,
            'hasShippingFee' => $hasShippingFee,
            'addresses' => $user->addresses,
        ]);
    }

    public function send(Request $request)
    {
        $data = $this->validate($request, [
            'contact_name' => 'required|string',
            'phone' => 'required|digits_between:7,11',
            'address' => 'required|string',
        ]);

        $session = $request->session();
        $ids = $session->pull('purchasing_candidates');
        $user = $request->user();
        $shoppingCart = $user->shoppingCart;
        $list = collect($ids)
            ->filter(function ($id) use ($shoppingCart) {
                return $shoppingCart->contains($id);
            })
            ->map(function ($id) use ($shoppingCart) {
                return $shoppingCart->firstWhere('id', $id);
            });
        $total = $list->sum(function ($cartItem) {
            return $cartItem->book->price * $cartItem->amount;
        });
        if ($total < 69) {
            $total += 8;
        }

        $order = new Order();
        $order->contact_name = $data['contact_name'];
        $order->phone = $data['phone'];
        $order->address = $data['address'];
        $order->user_id = auth()->id();
        $order->status = 'paying';
        $order->price = $total;
        $order->save();

        $list->each(function ($cartItem) use ($order, $user) {
            $order->books()->attach($cartItem->book->id, [
                'amount' => $cartItem->amount,
                'user_id' => $user->id,
            ]);

            $cartItem->delete();
        });

        $payment = [
            'out_trade_no' => $order->id,
            'total_amount' => $total,
            'subject' => '中国淘书网订单 - '.$order->id,
        ];

        CheckOrderPaymentStatus::dispatch(
            Order::findOrFail($order->id),
            $this->config()
        )->delay(now()->addSeconds(30));

        return Pay::alipay($this->config())->web($payment);
    }

    public function return()
    {
        $response = Pay::alipay($this->config())->verify();

        CheckOrderPaymentStatus::dispatch(
            Order::findOrFail($response['out_trade_no']),
            $this->config()
        );

        return view('pay.created');
    }

    public function notify()
    {
        $payment = Pay::alipay($this->config());

        try {
            $response = $payment->verify();

            if (
                $response['trade_status'] === 'TRADE_SUCCESS' ||
                $response['trade_status'] === 'TRADE_FINISHED'
            ) {
                $order = Order::findOrFail($response['out_trade_no']);
                $order->paid_at = $response['send_pay_date'];
                $order->status = 'paid';
                $order->save();
            }
        } catch (\Exception $e) {
            //
        }

        return $pay->success();
    }

    protected function config()
    {
        $config = config('alipay');
        $privateKey = file_get_contents($config['private_key']);
        $privateKey = preg_replace('/---.*---/', '', $privateKey);
        $privateKey = str_replace("\n", '', $privateKey);
        $config['private_key'] = $privateKey;

        return $config;
    }
}
