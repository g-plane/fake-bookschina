<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsManagementController extends Controller
{
    public function list(Request $request)
    {
        $perPage = $request->input('per-page', 10);

        return Comment::paginate($perPage);
    }

    public function content(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'content' => 'required|string',
        ]);

        Comment::where('id', $data['id'])
            ->update(['content' => $data['content']]);

        return response()->json(['code' => 0]);
    }

    public function rating(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'rating' => 'required|string',
        ]);

        Comment::where('id', $data['id'])
            ->update(['rating' => $data['rating']]);

        return response()->json(['code' => 0]);
    }

    public function delete(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        Comment::where('id', $id)->delete();

        return response()->json(['code' => 0]);
    }
}
