<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCart;
use Illuminate\Http\Request;

class ShoppingCartController extends Controller
{
    public function list(Request $request)
    {
        return $request->user()
            ->shoppingCart
            ->map(function ($cart) {
                $cart->book = $cart->book()->select([
                    'id',
                    'name',
                    'author',
                    'price',
                    'img',
                ])->first();
                return $cart;
            });
    }

    public function add(Request $request)
    {
        ['book' => $book] = $this->validate($request, [
            'book' => 'required|integer|exists:books,id',
            'amount' => 'integer',
        ]);
        $amount = intval($request->input('amount', 1));

        $user = $request->user();
        if ($user->shoppingCart()->where('book_id', $book)->count() === 0) {
            ShoppingCart::create([
                'user_id' => $user->id,
                'book_id' => $book,
                'amount' => 1,
            ]);
        } else {
            $user->shoppingCart()
                ->where('book_id', $book)
                ->first()
                ->increment('amount', $amount);
        }

        return response()->json([
            'code' => 0,
            'amount' => $user->shoppingCart()->where('book_id', $book)->first()->amount,
        ]);
    }

    public function remove(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $user = $request->user();
        $user->shoppingCart()->findOrFail($id)->delete();

        return response()->json(['code' => 0]);
    }

    public function decrement(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
            'amount' => 'integer',
        ]);
        $amount = intval($request->input('amount', 1));

        $user = $request->user();
        $cart = $user->shoppingCart()->findOrFail($id);
        if ($cart->amount != 0) {
            $cart->amount -= $amount;
            $cart->save();
        }

        return response()->json(['code' => 0, 'amount' => $cart->amount]);
    }

    public function setAmount(Request $request)
    {
        ['id' => $id, 'amount' => $amount] = $this->validate($request, [
            'id' => 'required|integer',
            'amount' => 'required|integer',
        ]);

        $cart = $request->user()->shoppingCart()->findOrFail($id);
        if ($amount > 0) {
            $cart->amount = $amount;
            $cart->save();
        }

        return response()->json(['code' => 0, 'amount' => $cart->amount]);
    }
}
