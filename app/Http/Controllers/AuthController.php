<?php

namespace App\Http\Controllers;

use Gregwar\Captcha\CaptchaBuilder;

class AuthController extends Controller
{
    public function captcha(CaptchaBuilder $builder)
    {
        $builder->build();
        session(['captcha' => $builder->getPhrase()]);
        return response($builder->output(), 200, [
            'Content-Type' => 'image/jpeg',
            'Cache-Control' => 'no-store',
        ]);
    }
}
