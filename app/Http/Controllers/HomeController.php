<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', 0)->get();
        $newCreated = Book::latest()->take(6)->get();
        $newPublished = Book::latest('publish_time')->take(6)->get();

        $goodRatingBooksId = DB::table('comments')
            ->select(DB::raw('count(`rating`) as ratings_count'))
            ->addSelect('book_id')
            ->where('rating', 'good')
            ->orderBy('ratings_count', 'desc')
            ->groupBy('book_id')
            ->take(6)
            ->get()
            ->pluck('book_id')
            ->all();
        $goodRating = Book::whereIn('id', $goodRatingBooksId)->get();

        return view('index', [
            'categories' => $categories,
            'topics' => [
                'newCreated' => [
                    'first' => $newCreated[0],
                    'rest' => $newCreated->skip(1),
                ],
                'newPublished' => [
                    'first' => $newPublished[0],
                    'rest' => $newPublished->skip(1),
                ],
                'goodRating' => [
                    'first' => $goodRating[0],
                    'rest' => $goodRating->skip(1),
                ],
            ],
        ]);
    }
}
