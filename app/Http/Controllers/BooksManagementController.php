<?php

namespace App\Http\Controllers;

use Cache;
use Storage;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class BooksManagementController extends Controller
{
    public function list(Request $request)
    {
        $perPage = $request->input('per-page', 10);

        $pagination = Book::paginate($perPage);
        $collection = $pagination
            ->getCollection()
            ->map(function ($book) {
                $book->category_name = $book->category ? $book->category->name : '无分类';

                return $book;
            });

        return tap($pagination)->setCollection($collection);
    }

    public function info(Request $request)
    {
        return Book::findOrFail($request->input('id'));
    }

    public function add(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'author' => 'required|string',
            'amount' => 'required|integer|min:0',
            'price' => 'required|numeric|min:0',
            'category_id' => 'required|integer',
            'isbn' => 'required|digits:13',
            'publisher' => 'required|string',
            'publish_time' => 'required|date',
            'img' => 'string|nullable',
            'description' => 'string|nullable',
            'detail' => 'string|nullable',
        ]);

        $category = $data['category_id'];
        if ($category != 0 && Category::where('id', $category)->doesntExist()) {
            return response()->json(['code' => 1, 'message' => '分类不存在']);
        }

        $book = Book::create(Arr::except($data, 'id'));
        return response()->json(['code' => 0, 'id' => $book->id]);
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'name' => 'required|string',
            'author' => 'required|string',
            'amount' => 'required|integer|min:0',
            'price' => 'required|numeric|min:0',
            'category_id' => 'required|integer',
            'isbn' => 'required|digits:13',
            'publisher' => 'required|string',
            'publish_time' => 'required|date',
            'img' => 'string|nullable',
            'description' => 'string|nullable',
            'detail' => 'string|nullable',
        ]);

        $category = $data['category_id'];
        if ($category != 0 && Category::where('id', $category)->doesntExist()) {
            return response()->json(['code' => 1, 'message' => '分类不存在']);
        }

        $book = Book::find($data['id']);
        if (! $book) {
            return response()->json(['code' => 1, 'message' => '书本不存在']);
        }

        $book->update(Arr::except($data, 'id'));
        Cache::forget('book_model-'.$data['id']);
        return response()->json(['code' => 0]);
    }

    public function cover(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'file' => 'required|file|mimetypes:image/png,image/jpeg',
        ]);

        $book = Book::findOrFail($data['id']);

        Storage::putFileAs('covers', $request->file, $data['id'].'.jpg');
        $book->img = config('app.url').':88/'.$data['id'].'.jpg';
        $book->save();
        Cache::forget('book_model-'.$data['id']);

        return response('', 200);
    }

    public function remove(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        Book::where('id', $data['id'])->delete();
        Cache::forget('book_model-'.$data['id']);

        return response()->json(['code' => 0]);
    }
}
