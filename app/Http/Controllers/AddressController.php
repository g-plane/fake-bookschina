<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function list()
    {
        return auth()->user()->addresses;
    }

    public function add(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|digits_between:7,11',
        ]);

        $address = new Address();
        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->phone = $data['phone'];
        auth()->user()->addresses()->save($address);

        return response()->json(['code' => 0, 'id' => $address->id]);
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|digits_between:7,11',
        ]);

        $address = Address::find($data['id']);
        if (empty($address)) {
            return response()->json(['code' => 1, 'message' => '条目不存在']);
        }

        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->phone = $data['phone'];
        $address->save();

        return response()->json(['code' => 0]);
    }

    public function remove(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        Address::where('id', $id)->delete();

        return response()->json(['code' => 0]);
    }
}
