<?php

namespace App\Http\Controllers\Auth;

use App\Rules\Captcha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|email|exists:users',
            'password' => 'required|string|min:8',
            'captcha' => ['required', new Captcha],
        ]);
    }
}
