<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrdersManagementController extends Controller
{
    public function list(Request $request)
    {
        $perPage = $request->input('per-page', 10);

        return Order::paginate($perPage);
    }

    public function books(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required',
        ]);

        return Order::findOrFail($id)->books;
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
            'status' => 'required',
        ]);

        $order = Order::find($data['id']);
        if (empty($order)) {
            return response()->json(['code' => 1, 'message' => '订单不存在']);
        }

        if ($order->status === 'cancelled') {
            return response()->json([
                'code' => 1,
                'message' => '订单已被取消，不能修改其状态',
            ]);
        }

        $order->status = $data['status'];
        $order->save();

        return response()->json(['code' => 0]);
    }

    public function cancel(Request $request)
    {
        ['id' => $id] = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        Order::where('id', $id)->update(['status' => 'cancelled']);

        return response()->json(['code' => 0]);
    }
}
