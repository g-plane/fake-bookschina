<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = auth()->user()->orders()->paginate(7);

        return view('user.orders', compact('orders'));
    }

    public function detail($id)
    {
        $order = Order::findOrFail($id);
        if ($order->user_id != auth()->id()) {
            return abort(404);
        }

        return view('user.order', compact('order'));
    }

    public function cancel(Request $request)
    {
        $data = $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $order = Order::findOrFail($data['id']);
        if ($order->status !== 'paying') {
            abort(403, '未处于「待付款」状态的订单不能被取消。');
        }

        $order->status = 'cancelled';
        $order->save();

        return back();
    }
}
