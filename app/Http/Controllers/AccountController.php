<?php

namespace App\Http\Controllers;

use Hash;
use App\Models\Address;
use App\Models\ShoppingCart;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function password(Request $request)
    {
        $data = $this->validate($request, [
            'old_password' => 'required|string',
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        $user = auth()->user();

        if (! Hash::check($data['old_password'], $user->password)) {
            return back()->with('message', '原密码不正确');
        }

        $user->password = Hash::make($data['new_password']);
        $user->save();

        auth()->logout();
        return redirect('/')->with('message', '密码修改成功，请重新登录');
    }

    public function delete(Request $request)
    {
        $data = $this->validate($request, [
            'password' => 'required|string',
        ]);
        $id = auth()->id();
        $user = auth()->user();

        if (! Hash::check($data['password'], $user->password)) {
            return back()->with('message', '密码不正确');
        }

        Address::where('user_id', $id)->delete();
        ShoppingCart::where('user_id', $id)->delete();
        $user->favorites()->delete();
        $user->delete();

        return redirect('/');
    }
}
