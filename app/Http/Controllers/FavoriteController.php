<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    public function list(Request $request)
    {
        return $request->user()->favorites;
    }

    public function add(Request $request)
    {
        ['book' => $book] = $this->validate($request, [
            'book' => 'required|integer',
        ]);

        $user = $request->user();
        if ($user->favorites->contains($book)) {
            return response()->json(['code' => 1, 'message' => '您已收藏过该书本']);
        }

        $user->favorites()->attach($book);
        if ($request->expectsJson()) {
            return response()->json(['code' => 0]);
        }
    }

    public function remove(Request $request)
    {
        ['book' => $book] = $this->validate($request, [
            'book' => 'required|integer',
        ]);

        $request->user()->favorites()->detach($book);
        return response()->json(['code' => 0]);
    }
}
