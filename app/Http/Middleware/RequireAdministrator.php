<?php

namespace App\Http\Middleware;

use Closure;

class RequireAdministrator
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user->is_admin) {
            return $next($request);
        }

        abort(403);
    }
}
