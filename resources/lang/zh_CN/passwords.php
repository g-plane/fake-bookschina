<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '密码已重置',
    'sent' => '包含重置密码链接的电子邮件已发送',
    'token' => '重置密码所需的 token 无效',
    'user' => "找不到使用该电子邮箱地址的用户",

];
