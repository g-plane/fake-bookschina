@extends('auth.base')

@section('title', '找回密码')

@section('content')
<h3>找回密码</h3>
<form action="/auth/forget" method="post">
    @if ($errors->any())
        <div class="error-msg">{{ $errors->first() }}</div>
    @endif
    @if ($status = session()->pull('status'))
        <div class="error-msg">{{ $status }}</div>
    @endif
    @csrf
    <div>
        <label>
            <span class="key">电子邮箱：</span>
            <input type="email" name="email" required autocomplete="off">
        </label>
    </div>
    <div>
        <label>
            <span class="key">验证码：</span>
            <input type="text" name="captcha" required autocomplete="off">
            <img src="/auth/captcha?time={{ time() }}" class="captcha" alt="captcha" title="单击更换验证码">
        </label>
    </div>
    <div>
        <span class="key"></span>
        <input type="submit" class="submit" value="提交">
    </div>
</form>
@endsection
