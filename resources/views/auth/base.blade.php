<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') | 网上图书商店</title>
    <link rel="stylesheet" href="{{ mix('/css/auth.css') }}">
</head>
<body>
    <header>
        <a href="/"><img src="{{ mix('/images/logo.png') }}"></a>
        <div class="features">
            <span class="circle">正</span>
            <span class="text">正版好图书</span>
            <span class="circle">邮</span>
            <span class="text">全场满69包邮</span>
            <span class="circle">折</span>
            <span class="text">特价书一折起</span>
        </div>
    </header>
    <div class="login-box">
        @yield('content')
    </div>
    <script src="{{ mix('/js/captcha.js') }}"></script>
</body>
</html>
