@extends('auth.base')

@section('title', '注册')

@section('content')
<h3>新用户注册</h3>
<span class="login-tip">
    已有账号？
    <a href="/auth/login">请登录</a>
</span>
<form action="/auth/reg" method="post">
    @if ($errors->any())
        <div class="error-msg">{{ $errors->first() }}</div>
    @endif
    @csrf
    <div>
        <label>
            <span class="key">用户名：</span>
            <input type="text" name="username" required autocomplete="off">
        </label>
    </div>
    <div>
        <label>
            <span class="key">电子邮箱：</span>
            <input type="email" name="email" required autocomplete="off">
        </label>
    </div>
    <div>
        <label>
            <span class="key">密码：</span>
            <input type="password" name="password" required minlength="8">
        </label>
    </div>
    <div>
        <label>
            <span class="key">密码确认：</span>
            <input type="password" name="password_confirmation" required minlength="8">
        </label>
    </div>
    <div>
        <label>
            <span class="key">验证码：</span>
            <input type="text" name="captcha" autocomplete="off" required>
            <img src="/auth/captcha?time={{ time() }}" class="captcha" alt="captcha" title="单击更换验证码">
        </label>
    </div>
    <div>
        <span class="key"></span>
        <input type="submit" class="submit" value="立即注册">
    </div>
</form>
@endsection
