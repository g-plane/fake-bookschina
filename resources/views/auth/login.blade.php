@extends('auth.base')

@section('title', '用户登录')

@section('content')
<h3>用户登录</h3>
<form action="/auth/login" method="post">
    @if ($errors->any())
        <div class="error-msg">{{ $errors->first() }}</div>
    @endif
    @csrf
    <div>
        <label>
            <span class="key">电子邮箱：</span>
            <input type="email" name="email" required autocomplete="off">
        </label>
    </div>
    <div>
        <label>
            <span class="key">密码：</span>
            <input type="password" name="password" required minlength="8">
        </label>
    </div>
    <div>
        <label>
            <span class="key">验证码：</span>
            <input type="text" name="captcha" required autocomplete="off">
            <img src="/auth/captcha?time={{ time() }}" class="captcha" alt="captcha" title="单击更换验证码">
        </label>
    </div>
    <div>
        <label>
            <span class="key"></span>
            <input type="checkbox" name="remember">
            <span>记住我</span>
        </label>
    </div>
    <div>
        <span class="key"></span>
        <input type="submit" class="submit" value="登录">
    </div>
</form>
<div class="links">
    <a href="/auth/forget">忘记密码？</a>
    <a href="/auth/reg" style="float: right">新用户注册</a>
</div>
@endsection
