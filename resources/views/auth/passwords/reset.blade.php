@extends('auth.base')

@section('title', '重置密码')

@section('content')
<h3>重置密码</h3>
<form action="/auth/reset" method="post">
    @if ($errors->any())
        <div class="error-msg">{{ $errors->first() }}</div>
    @endif
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <input type="hidden" name="email" value="{{ $email }}">
    <div>
        <label>
            <span class="key">新密码：</span>
            <input type="password" name="password" required minlength="8">
        </label>
    </div>
    <div>
        <label>
            <span class="key">密码确认：</span>
            <input type="password" name="password_confirmation" required minlength="8">
        </label>
    </div>
    <div>
        <span class="key"></span>
        <input type="submit" class="submit" value="提交">
    </div>
</form>
@endsection
