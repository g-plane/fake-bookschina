@extends('faq.toNews.base')

@section('content')
	<div>
		<h4>第一步 登录或注册</h4>
		<p>输入您的电子邮箱密码，进入首页。如果没有账号，请点击“新用户注册” 。</p>
		<img src="https://s2.ax1x.com/2019/10/12/uOqOfS.jpg">
	</div>
	<br><br>
	<div>
		<h4>第二步 选择商品</h4>
		<p>您可以通过各主题书店、图书分类、或搜索图书（输入图书关键字），找到自己喜欢的商品。</p>
		<img src="https://s2.ax1x.com/2019/10/12/uOqjSg.jpg">
	</div>
	<br><br>
	<div>
		<h4>第三步 把商品放入购物车</h4>
		<p>查看商品，了解详细信息；点击购买，把商品放入购物车；通过搜索或图书分类，主题书店继续购买其他商品。</p>
		<img src="https://s2.ax1x.com/2019/10/12/uOqqFf.jpg">
	</div>
	<br><br>
	<div>
		<h4>第四步 进入购物车</h4>
		<p>选择购物车里会显示您所购买图书的价格、数量，您可以修改数量或删除，也可以把图书移入收藏夹中，以后再购买。</p>
		<img src="https://s2.ax1x.com/2019/10/12/uOqHTP.jpg">
	</div>
	<br><br>
	<div>
		<h4>第五步 确认收货信息，结算并支付</h4>
		<img src="https://s2.ax1x.com/2019/10/12/uOqLY8.jpg">
	</div>
	<br><br>
	<br><br>
@endsection

