<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('/css/toNews.css') }}" />
    <title>@yield('title', '帮助中心') - 中国淘书网</title>
</head>
<body>
@include ('common.header')

<div class="content">
    <section class="sidebar">
        <ul>
            <li>帮助中心</li>
            <li><a href="/faq/otherInfo/about">关于我们</a></li>
            <li><a href="/faq/toNews/question">订购问题</a></li>
            <li><a href="#">支付问题</a></li>
            <li><a href="/faq/Buy/peison">配送问题</a></li>
            <li><a href="#">售后问题</a></li>
            <li><a href="#">会员等级和积分问题</a></li>
            <li><a href="#">发票问题</a></li>
            <li><a href="/faq/toNews/price">关于特价书的常见问题</a></li>
            <li><a href="#">购物演示</a></li>
            <li><a href="#">团购服务</a></li>
            <li><a href="#">批发说明</a></li>
        </ul>
    </section>
    <div>
        @yield('content')
    </div>
</div>

@include ('common.footer')

<script src="{{ mix('/js/toNews/sidebar.js') }}"></script>
</body>
</html>
