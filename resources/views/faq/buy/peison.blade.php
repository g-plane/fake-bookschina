@extends('faq.toNews.base')

@section('content')
	<div class="wrapp">
		<div class="t1">
			配送问题
		</div>
		<div>
			<p class="t2">全国快递运费6元/单，满69元免运费！ </p>
            
            <p class="t3">特殊区域：港澳台及海外地区运费为图书定价的100％-180％<br>
            特殊免运费方式：9.9包邮区、淘书团频道全场包邮</p>
		</div>
		<div><br>
			<p class="t2">按区域查询：</p>
            
            <p class="t4">请您根据收货人地址选择正确的省、市、区/县后，系统会提示您可供选择的送货方式及相关配送信息。</p>

            <select>
            	<option selected="selected" value="中国">中国</option>
            	<option value="其他国家">其他国家</option>
            </select>
            <select>
            	<option value="请选择-0">请选择</option>
                <option value="北京-110000">北京</option>
                <option value="天津-120000">天津</option>
                <option value="河北-130000">河北</option>
                <option value="山西-140000">山西</option>
                <option value="内蒙古-150000">内蒙古</option>
                <option value="辽宁-210000">辽宁</option>
                <option value="吉林-220000">吉林</option>
                <option value="黑龙江-230000">黑龙江</option>
                <option value="上海-310000">上海</option>
                <option value="江苏-320000">江苏</option>
                <option value="浙江-330000">浙江</option>
                <option value="安徽-340000">安徽</option>
                <option value="福建-350000">福建</option>
                <option value="江西-360000">江西</option>
                <option value="山东-370000">山东</option>
                <option value="河南-410000">河南</option>
                <option value="湖北-420000">湖北</option>
                <option value="湖南-430000">湖南</option>
                <option value="广东-440000">广东</option>
                <option value="广西-450000">广西</option>
                <option value="海南-460000">海南</option>
                <option value="重庆-500000">重庆</option>
                <option value="四川-510000">四川</option>
                <option value="贵州-520000">贵州</option>
                <option value="云南-530000">云南</option>
                <option value="西藏-540000">西藏</option>
                <option value="陕西-610000">陕西</option>
                <option value="甘肃-620000">甘肃</option>
                <option value="青海-630000">青海</option>
                <option value="宁夏-640000">宁夏</option>
                <option value="新疆-650000">新疆</option>
                <option value="台湾-710000">台湾</option>
                <option value="香港-810000">香港</option>
                <option value="澳门-820000">澳门</option>
            </select>
            <select>
            	<option value="0">请选择</option>
            </select>
            <select>
            	<option value="0">请选择</option>
            </select>
		</div>
	</div>
	<div>
		
	</div>

@endsection