@extends('book.base')

@section('title', '搜索结果')

@section('css')
<link rel="stylesheet" href="{{ mix('/css/list.css') }}" />
@endsection

@section('content')
<ul class="books-list">
    @foreach ($books as $book)
    <li>
        <div class="cover">
            <img src="{{ $book->img }}" alt="{{ $book->name }}">
        </div>
        <div>
            <div>
                <a class="book-name" href="/book/{{ $book->id }}" target="_blank">
                    {{ $book->name }}
                </a>
            </div>
            <div class="book-info">
                <div>
                    <div class="book-meta">
                        {{ $book->author }} / {{ $book->publish_date }} / {{ $book->publisher }}
                    </div>
                    <div class="price">￥{{ $book->price }}</div>
                    <div class="description">{{ $book->description }}</div>
                </div>
                <div>
                    <button class="btn btn-add-to-cart" data-book-id="{{ $book->id }}">
                        放入购物车
                    </button>
                    &nbsp;&nbsp;
                    @if ($favorites->contains($book->id))
                        <button class="btn btn-fav" data-book-id="{{ $book->id }}" data-fav="true">
                            取消收藏
                        </button>
                    @else
                        <button class="btn btn-fav" data-book-id="{{ $book->id }}">收藏</button>
                    @endif
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>

<div>
    {{ $books->links() }}
</div>
@endsection

@section('js')
<script src="{{ mix('/js/books-list.js') }}"></script>
@endsection
