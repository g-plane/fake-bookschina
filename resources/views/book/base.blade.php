<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | 中国淘书网</title>
    <link rel="stylesheet" href="{{ mix('/css/rightNav.css') }}">
    @yield('css')
</head>
<body>
    @include('common.header')

    <section class="content">
        @yield('content')
    </section>

    @include('common.rightNav')
    @include('common.footer')

    @yield('js')
</body>
</html>
