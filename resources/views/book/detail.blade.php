@extends('book.base')

@section('title', $book->name)

@section('css')
<link rel="stylesheet" href="{{ mix('/css/detail.css') }}">
@endsection

@section('content')
<div class="breadcrumb">
    <div class="breadcrumb-item">
        <a href="/">中国淘书网</a>
    </div>
    <div class="breadcrumb-item separator">
        <i class="el-icon-arrow-right"></i>
    </div>
    @foreach ($categories as $category)
        <div class="breadcrumb-item">
            <a href="/search?category={{ $category['id'] }}">
                {{ $category['name'] }}
            </a>
        </div>
        <div class="breadcrumb-item separator">
            <i class="el-icon-arrow-right"></i>
        </div>
    @endforeach
    <div class="breadcrumb-item">{{ $book->name }}</div>
</div>
<div class="detail">
    <div class="book-img">
        <img src="{{ $book->img }}" alt="{{ $book->name }}">
    </div>
    <div class="book-info">
        <h1 class="book-name">{{ $book->name }}</h1>
        <div class="book-meta">
            <p>
                作者：
                <a href="/search?author={{ $book->author }}">
                    {{ $book->author }}
                </a>
            </p>
            <p>
                出版社：
                <a href="/search?publisher={{ $book->publisher }}">
                    {{ $book->publisher }}
                </a>
            </p>
            <p>出版时间：{{ $book->publish_time->format('Y-m-d') }}</p>
            <p>
                价格：<span class="price">￥{{ $book->price }}</span>
            </p>
        </div>
        <div class="actions">
            <button id="to-cart">加入购物车</button>
            <button id="to-fav" data-fav="{{ $favored ? 'true' : '' }}">
                {{ $favored ? '取消收藏' : '收藏' }}
            </button>
        </div>
    </div>
</div>

@php
    $openComments = session()->pull('open-comments');
@endphp
<section class="main-content">
    <div class="book-detail">
        <div data-selected="detail" class="book-tab">
            <div class="{{ $openComments ? '' : 'active' }}">图书详情</div>
            <div class="{{ $openComments ? 'active' : '' }}">评论</div>
        </div>

        <div class="detail-content {{ $openComments ? 'hidden' : '' }}">
            {!! $book->detail !!}
        </div>

        <div class="comments {{ $openComments ? '' : 'hidden' }}">
            <ul class="comments-list">
                @foreach ($comments as $comment)
                    <li>
                        <p class="meta">
                            {{ $comment->author ? $comment->author->username : '账号已注销' }}：
                            <span class="rating-{{ $comment->rating }}">
                                {{ $ratingLabels[$comment->rating] }}
                            </span>
                        </p>
                        <p class="comment-content">
                            @foreach(explode("\n", $comment->content) as $line)
                                {{ $line }}<br>
                            @endforeach
                        </p>
                        <p><time>{{ $comment->created_at }}</time></p>
                    </li>
                @endforeach
            </ul>
            @if ($canComment)
                <form action="/book/{{ $book->id }}/comment" method="post">
                    <p class="heading">添加评论：</p>
                    @csrf
                    <textarea name="content" rows="6"></textarea>
                    <div class="ratings">
                        评价：
                        <label>
                            <input type="radio" name="rating" value="bad">
                            <span class="rating-bad">差评</span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="medium">
                            <span class="rating-medium">中评</span>
                        </label>
                        <label>
                            <input type="radio" name="rating" value="good" checked="checked">
                            <span class="rating-good">好评</span>
                        </label>
                    </div>
                    <input type="submit" value="提交">
                    @if ($errors->any())
                        <div class="errors">{{ $errors->first() }}</div>
                    @endif
                </form>
            @endif
        </div>
    </div>
</section>

<div class="you-may-like">
    <div class="title">猜您喜欢</div>
    <ul>
        @foreach ($youMayLike as $book)
            <li>
                <a href="/book/{{ $book->id }}" target="_blank">
                    <img src="{{ $book->img }}" alt="{{ $book->name }}">
                    <p class="book-name">{{ $book->name }}</p>
                    <p class="author">{{ $book->author }} 著</p>
                    <p class="price">￥{{ $book->price }}</p>
                </a>
                <button class="to-cart" data-book-id="{{ $book->id }}">
                    加入购物车
                </button>
            </li>
        @endforeach
    </ul>
</div>
@endsection

@section('js')
<script src="{{ mix('/js/book-detail.js') }}"></script>
@endsection
