<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>淘书网管理系统</title>
</head>
<body>
    <div id="app"></div>
    <script src="{{ mix('/js/admin.js') }}"></script>
</body>
</html>
