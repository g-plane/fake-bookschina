@extends('user.base')

@section('title', '订单详情')

@section('content')
<link rel="stylesheet" href="{{ mix('/css/user-orders.css') }}">

<main id="detail-page">
    <h3>订单详情</h3>
    <p>订单号：{{ $order->id }}</p>
    <p>收货人姓名：{{ $order->contact_name }}</p>
    <p>收货地址：{{ $order->address }}</p>
    <p>收货人电话：{{ $order->phone }}</p>
    <p>
        订单状态：
        @switch($order->status)
            @case('paying')
                待付款
                @break
            @case('paid')
                待发货
                @break
            @case('shipping')
                快递正在运输
                @break
            @case('received')
                订单已完成
                @break
            @case('cancelled')
                订单已取消
                @break
        @endswitch
    </p>
    @unless ($order->status === 'paying')
        <p>支付时间：{{ $order->paid_at }}</p>
    @endunless
    <p>订单金额：￥{{ $order->price }}</p>
    <p>订单创建时间：{{ $order->created_at }}</p>

    <ul class="detail-books">
        @foreach ($order->books as $book)
            <li>
                <a href="/book/{{ $book->id }}" target="_blank">
                    <img class="cover" src="{{ $book->img }}" alt="{{ $book->name }}">
                </a>
                <span class="book-name">
                    <a href="/book/{{ $book->id }}" target="_blank" class="book-name">
                        {{ $book->name }}
                    </a>
                    <span>（{{ $book->author }} 著）</span>
                </span>
                <span>× {{ $book->pivot->amount }}</span>
                <span class="price">￥{{ $book->price * $book->pivot->amount }}</span>
            </li>
        @endforeach
    </ul>
</main>
@endsection
