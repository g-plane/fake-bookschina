@extends('user.base')

@section('title', '账户安全')

@section('content')
<main>
    @if (Session::has('message'))
        <div class="message">{{ Session::pull('message') }}</div>
    @endif
    @if ($errors->any())
        <div class="message">{{ $errors->first() }}</div>
    @endif

    <div class="container">
        <h4>修改密码</h4>
        <form action="/user/account/password" method="post">
            @csrf
            <label>
                <span>原密码：</span>
                <input type="password" name="old_password" required>
            </label>
            <label>
                <span>新密码：</span>
                <input type="password" name="new_password" minlength="8" required>
            </label>
            <label>
                <span>新密码确认：</span>
                <input type="password" name="new_password_confirmation" minlength="8" required>
            </label>
            <div><input type="submit" value="修改密码"></div>
        </form>
    </div>

    <div class="container">
        <h4>账户删除</h4>
        <p>如果需要，可以在这里删除您的账户以及相关数据（如购物车、收藏等），但订单和评论会被保留。</p>
        <p>此操作不可撤销。如果确定要删除，请在下方输入您的密码：</p>
        <form action="/user/account/delete" method="post">
            @csrf
            <label><input type="password" name="password"></label>
            <label><input type="submit" value="确定删除账户"></label>
        </form>
    </div>
</main>

<link rel="stylesheet" href="{{ mix('/css/user-account.css') }}">
@endsection
