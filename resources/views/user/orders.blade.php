@extends('user.base')

@section('title', '我的订单')

@section('content')
<link rel="stylesheet" href="{{ mix('/css/user-orders.css') }}">

<ul id="orders">
    @foreach ($orders as $order)
        <li>
            <div class="order-meta">
                <span>订单号：{{ $order->id }}</span>
                <span>总价：<span class="price">￥{{ $order->price }}</span></span>
            </div>
            <div class="detail">
                <ul class="books">
                    @foreach ($order->books as $book)
                        <li>
                            <a href="/book/{{ $book->id }}" target="_blank">
                                <img
                                    class="cover"
                                    src="{{ $book->img }}"
                                    alt="{{ $book->name }}"
                                >
                            </a>
                            <a href="/book/{{ $book->id }}" target="_blank" class="book-name">
                                {{ $book->name }}
                            </a>
                            <span>（{{ $book->author }} 著）</span>
                        </li>
                    @endforeach
                </ul>
                <div class="status">
                    @switch($order->status)
                        @case('paying')
                            状态：待付款
                            @break
                        @case('paid')
                            状态：待发货
                            @break
                        @case('shipping')
                            状态：快递正在运输
                            @break
                        @case('received')
                            状态：订单已完成
                            @break
                        @case('cancelled')
                            状态：订单已取消
                            @break
                    @endswitch
                </div>

                <a
                    href="/user/orders/{{ $order->id }}"
                    class="link-detail"
                >订单详情</a>

                @if ($order->status === 'paying')
                    <form action="/user/orders/cancel" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $order->id }}">
                        <input
                            type="submit"
                            value="取消订单"
                            title="此操作不可撤销，请谨慎"
                            class="btn-cancel"
                        >
                    </form>
                @endif
            </div>
        </li>
    @endforeach
</ul>
{{ $orders->links() }}
@endsection
