<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | 中国淘书网</title>
    <link rel="stylesheet" href="{{ mix('/css/user.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/rightNav.css') }}">
</head>
<body>
    @include('common.header')

    <section class="content">
        <ul class="user-nav">
            <li><a href="/user/orders">我的订单</a></li>
            <li><a href="/user/favorites">我的收藏</a></li>
            <li><a href="/user/address">地址管理</a></li>
            <li><a href="/user/account">账户安全</a></li>
        </ul>

        <div>
            @yield('content')
        </div>
    </section>

    @include('common.rightNav')
    @include('common.footer')
    <script src="{{ mix('/js/user.js') }}"></script>
</body>
</html>
