<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('/css/index.css') }}" />
    <link rel="stylesheet" href="{{ mix('/css/rightNav.css') }}" />
    <title>中国淘书网</title>
    <link rel="preload" as="image" href="http://bookschina.test:88/banner/1.png">
    <link rel="preload" as="image" href="http://bookschina.test:88/banner/2.png">
    <link rel="preload" as="image" href="http://bookschina.test:88/banner/3.png">
    <link rel="preload" as="image" href="http://bookschina.test:88/banner/4.png">
    <link rel="preload" as="image" href="http://bookschina.test:88/banner/5.png">
</head>
<body>

    <div style="width: 100%">
        <img src="{{ mix('/images/topBanner.png') }}" style="width: 100%">
    </div>

    @include('common.header')

    <div class="main-content">
        <section class="heading-section">
            <ul class="categories">
                @foreach ($categories as $category)
                    <li>
                        <a href="/search?category={{ $category->id }}">
                            {{ $category->name }}
                            @unless ($category->children->isEmpty())
                                <img src="{{ mix('/images/arrow.png') }}" alt="right_arrow">
                            @endunless
                        </a>
                        @unless ($category->children->isEmpty())
                            <ul class="snd-level">
                                @foreach ($category->children as $child)
                                    <li>
                                        <a href="/search?category={{ $child->id }}">
                                            {{ $child->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="second-level-categories">
                                <ul>
                                    @foreach ($category->children as $child)
                                        <li>
                                            <p>
                                                <a href="/search?category={{ $child->id }}">
                                                    {{ $child->name }}
                                                </a>
                                            </p>
                                            @foreach ($child->children as $c)
                                                <a class="tri" href="/search?category={{ $c->id }}">
                                                    {{ $c->name }}
                                                </a>
                                            @endforeach
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>

            <div style="margin-left: 5px;">
                <div class="banner">
                    <a class="banner-picture" href="#">
                        <img src="http://bookschina.test:88/banner/1.png">
                    </a>
                    <div class="banner-links">
                        <a class="banner-title" data-img="http://bookschina.test:88/banner/1.png" href="/book/1">攀登者</a>
                        <a class="banner-title" data-img="http://bookschina.test:88/banner/2.png" href="/book/2">半小时漫画中国史</a>
                        <a class="banner-title" data-img="http://bookschina.test:88/banner/3.png" href="/book/3">我在未来等你</a>
                        <a class="banner-title" data-img="http://bookschina.test:88/banner/4.png" href="/book/4">数据挖掘导论</a>
                        <a class="banner-title" data-img="http://bookschina.test:88/banner/5.png" href="/book/5">荒寺月落</a>
                    </div>
                </div>

                <div id="recommendation">
                    <ul>
                        <li class="active" data-topic="new-created">新品特惠</li>
                        <li data-topic="new-published">新书速递</li>
                        <li data-topic="good-rating">读者好评</li>
                    </ul>
                    @foreach ($topics as $topicName => $data)
                        <div
                            class="rec-content {{ $loop->first ? '' : 'hide' }}"
                            data-topic="{{ Str::kebab($topicName) }}"
                        >
                            <div class="first">
                                <a class="cover" href="/book/{{ $data['first']->id }}">
                                    <img
                                        src="{{ $data['first']->img }}"
                                        alt="{{ $data['first']->name }}"
                                    >
                                </a>
                                <div class="book-info">
                                    <p>
                                        <a
                                            class="book-name"
                                            href="/book/{{ $data['first']->id }}"
                                        >{{ $data['first']->name }}</a>
                                    </p>
                                    <p class="price">￥{{ $data['first']->price }}</p>
                                    <p class="description">{{ $data['first']->description }}</p>
                                </div>
                            </div>
                            <ul class="rest">
                                @foreach ($data['rest'] as $book)
                                    <li>
                                        <div class="cover">
                                            <img src="{{ $book->img }}" alt="{{ $book->name }}">
                                        </div>
                                        <p class="book-name">
                                            <a class="book-name" href="/book/{{ $book->id }}">{{ $book->name }}</a>
                                        </p>
                                        <p class="price">￥{{ $book->price }}</p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>

    @include('common.rightNav')
    @include('common.footer')

    <script src="{{ mix('/js/index.js') }}"></script>
    @if (Session::has('message'))
        <script>
            alert("{{ Session::pull('message') }}")
        </script>
    @endif
</body>
</html>
