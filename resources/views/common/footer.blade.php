<footer>
    <div>
        <dl>
            <dt>新手上路</dt>
            <dd><a href="/faq/toNews/buy">购买流程演示</a></dd>
            <dd><a href="/auth/reg">注册用户 更改注册信息</a></dd>
            <dd><a href="/faq/toNews/question">购物常见问题</a></dd>
            <dd><a href="/faq/toNews/price">关于特价书的常见问题</a></dd>
        </dl>

        <dl>
            <dt>购买问题</dt>
            <dd><a href="#">订单跟踪</a></dd>
            <dd><a href="#">账户余额 申请提现</a></dd>
            <dd><a href="/faq/Buy/peison">配送方式及费用、范围</a></dd>
            <dd><a href="#">集团购买</a></dd>
        </dl>

        <dl>
            <dt>售后服务</dt>
            <dd><a href="#">退换货流程</a></dd>
            <dd><a href="#">投诉与建议</a></dd>
        </dl>

        <dl>
            <dt>特色服务</dt>
            <dd><a href="#">会员等级与积分</a></dd>
            <dd><a href="#">中图书馨卡</a></dd>
        </dl>

        <dl>
            <dt>其他信息</dt>
            <dd><a href="/faq/otherInfo/about">本站介绍</a></dd>
            <dd><a href="#">招聘英才</a></dd>
        </dl>
    </div>
    <img src="{{ mix('/images/bottomBar.jpg') }}" id="bottomBar" alt="bottomBar">
</footer>
