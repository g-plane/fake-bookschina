<div class="topbar">
    <span>欢迎光临网上图书商店！</span>&nbsp;
    @auth
        {{ Auth::user()->username }}，您好！&nbsp;
        <form action="/auth/logout" method="post" class="logout-form">
            @csrf
            <button type="submit" class="btn-logout">退出</button>
        </form>
    @endauth
    @guest
        请&nbsp;<a href="/auth/login">登录</a>&nbsp;或&nbsp;
        <a href="/auth/reg">注册</a>
    @endguest
    <div class="right">
        <a href="/shoppingCart">购物车</a>&nbsp;&nbsp;
        <a href="/user">个人中心</a>
    </div>
</div>
<div class="search-area">
    <ul class="search-box">
        <a href="/"><img id="logo" src="{{ mix('/images/logo.png') }}" /></a>
        <form action="/search" method="get">
            <input type="text" name="q" class="search" value="{{ request('q') }}">
            <input type="submit" class="submit" value="搜索">
        </form>
        @if (request()->is('/'))
            <ul class="search-recommendation">
                <li>
                    <a href="/book/{{ $topics['newCreated']['first']->id }}">
                        {{ $topics['newCreated']['first']->name }}
                    </a>
                </li>
                @foreach ($topics['newCreated']['rest'] as $book)
                    <li>
                        <a href="/book/{{ $book->id }}">{{ $book->name }}</a>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
<nav class="header-nav">
    <div class="nav-items">
        @if (request()->is('/'))
            <span class="category-dropdown">图书分类</span>
        @endif
        <a class="active" href="/">首页</a>
        <a href="#">淘书团</a>
        <a href="/subjects/bestSelling">畅销榜</a>
        <a href="#">新上架</a>
        <a href="#">9块9包邮</a>
        <a href="#">出版社浏览</a>
        <a href="#">闲情雅趣</a>
        <a href="#">批发</a>
    </div>
</nav>
