<div class="right-nav">
    @auth
    <div class="right-nav-item">
        <div class="nav-item-icon">
            <a href="/user"><img src="{{ mix('/images/user.png') }}" width="25" height="25"></a>
        </div>
        <div class="nav-item-text" >
            <a href="/user">个人中心</a>
        </div>
    </div>

    <div class="right-nav-item">
        <div class="nav-item-icon">
            <a href="/shoppingCart"><img src="{{ mix('/images/cart.png') }}" width="25" height="25"></a>
        </div>
        <div class="nav-item-text">
            <a href="/shoppingCart">购物车</a>
        </div>
    </div>

    <div class="right-nav-item">
        <div class="nav-item-icon">
            <a href="/user/favorites"><img src="{{ mix('/images/favorite.png') }}" width="25" height="25"></a>
        </div>
        <div class="nav-item-text">
            <a href="/user/favorites">我的收藏</a>
        </div>
    </div>
    @endauth

    <div class="right-nav-item">
        <div class="nav-item-icon">
            <a href=""><img src="{{ mix('/images/upArrow.png') }}" width="25" height="25"></a>
        </div>
        <div class="nav-item-text">
            <a href="">返回顶部</a>
        </div>
    </div>
</div>
