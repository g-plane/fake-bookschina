<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', '购物车') | 中国淘书网</title>
    <link rel="stylesheet" href="{{ mix('/vendor/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/index.css') }}" />
</head>
<body>
    @include('common.header')

    <div class="content">
        @yield('content')
    </div>

    @include('common.footer')
</body>
</html>
