@extends('shoppingCart.base')

@section('content')
<div id="cart"></div>
<script src="{{ mix('/js/shoppingCart.js') }}"></script>
@endsection
