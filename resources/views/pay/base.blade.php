<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>结算与支付 | 中国淘书网</title>
    <link rel="stylesheet" href="{{ mix('/css/pay.css') }}">
</head>
<body>
    @include('common.header')

    <section class="content">
        @yield('content')
    </section>

    @include('common.footer')
</body>
</html>
