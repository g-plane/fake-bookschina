@extends('pay.base')

@section('content')
<form action="/pay/send" method="post" id="shipping">
    @csrf
    <h3>收货信息</h3>
    @if ($errors->any())
        <div class="validation-error">{{ $errors->first() }}</div>
    @endif
    <div class="contact">
        <a class="link-to-addresses" href="/user/address" target="_blank">管理已保存地址</a>
        <ul class="presets">
            @foreach ($addresses as $address)
                <li
                    data-name="{{ $address->name }}"
                    data-address="{{ $address->address }}"
                    data-phone="{{ $address->phone }}"
                >
                    <p>收货人：{{ $address->name }}</p>
                    <p>地址：{{ $address->address }}</p>
                    <p>电话：{{ $address->phone }}</p>
                </li>
            @endforeach
        </ul>
        <div>
            <label>
                <span>收货人姓名：</span>
                <input type="text" name="contact_name" required>
            </label>
        </div>
        <div>
            <label>
                <span>收货人电话：</span>
                <input type="text" name="phone" required minlength="7" maxlength="11">
            </label>
        </div>
        <div>
            <label>
                <span>收货地址：</span>
                <input type="text" name="address" required>
            </label>
        </div>
    </div>

    <div>
        <ul class="books">
            <li>
                <div class="table-header book-name">图书名称</div>
                <div class="table-header book-price">定价（元）</div>
                <div class="table-header amount">数量</div>
                <div class="table-header total-price">小计（元）</div>
            </li>
            @foreach ($list as $item)
                <li>
                    <div class="book-name">
                        <a href="/book/{{ $item->book->id }}" target="_blank">
                            <img src="{{ $item->book->img }}" alt="{{ $item->book->name }}">
                        </a>
                        <a href="/book/{{ $item->book->id }}" target="_blank">
                            {{ $item->book->name }}
                        </a>
                    </div>
                    <div class="book-price">￥{{ $item->book->price }}</div>
                    <div class="amount">{{ $item->amount }}</div>
                    <div class="total-price">
                        ￥{{ $item->book->price * $item->amount }}
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="summary">
        <div>
            <p>总价：<span class="price">￥{{ $total }}</span></p>
            @if ($hasShippingFee)
                <p class="fee-tip">含 8 元运费</p>
            @else
                <p class="fee-tip">满 69 元免运费</p>
            @endif
        </div>

        <input type="submit" value="结算并支付">
    </div>
</form>
<script src="{{ mix('/js/prepare-pay.js') }}"></script>
@endsection
