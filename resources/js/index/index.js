import { fromEvent } from 'rxjs'
import { filter, pluck, map } from 'rxjs/operators'

const bannerPicture = document.querySelector('.banner-picture')
const bannerPictureImg = bannerPicture.querySelector('img')
fromEvent(document.querySelector('.banner'), 'mouseover')
  .pipe(
    pluck('target'),
    filter(element => element.hasAttribute('data-img')),
    map(element => ({
      img: element.dataset.img,
      redirect: element.href,
    }))
  )
  .subscribe(({ img, redirect }) => {
    bannerPicture.href = redirect
    bannerPictureImg.src = img
  })

const recommendations = Array.from(document.querySelectorAll('.rec-content'))
fromEvent(document.querySelector('#recommendation > ul'), 'mouseover')
  .pipe(
    pluck('target'),
    filter(({ tagName }) => tagName === 'LI')
  )
  .subscribe(element => {
    document.querySelector('#recommendation .active').classList.remove('active')
    element.classList.add('active')

    recommendations.forEach(el => {
      if (el.dataset.topic === element.dataset.topic) {
        el.classList.remove('hide')
      } else {
        el.classList.add('hide')
      }
    })
  })

const categories = document.querySelector('.categories')
Array.from(document.querySelectorAll('.second-level-categories'))
  .forEach(el => {
    const left = categories.offsetLeft + categories.offsetWidth
    el.style.left = `${left}px`
    el.style.top = `${categories.offsetTop}px`
  })
