import Vue from 'vue'
import Router from 'vue-router'
import Users from './Users.vue'
import Books from './Books.vue'
import BookEditor from './BookEditor.vue'
import Categories from './Categories.vue'
import Orders from './Orders.vue'
import Comments from './Comments.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/users',
      component: Users,
    },
    {
      path: '/books',
      component: Books,
    },
    {
      path: '/books/add',
      component: BookEditor,
    },
    {
      path: '/books/edit/:id',
      component: BookEditor,
    },
    {
      path: '/categories',
      component: Categories,
    },
    {
      path: '/orders',
      component: Orders,
    },
    {
      path: '/comments',
      component: Comments,
    },
  ]
})
