import Vue from 'vue'
import List from './List.vue'

new Vue({
  el: '#cart',
  render: h => h(List)
})
