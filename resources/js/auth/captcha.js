const captcha = document.querySelector('.captcha')
captcha.addEventListener('click', () => {
  captcha.src = `/auth/captcha?time=${Date.now()}`
})
