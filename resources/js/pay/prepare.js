import { fromEvent, merge } from 'rxjs'
import { filter, pluck, map } from 'rxjs/operators'

const presetsElement = document.querySelector('.presets')
const target$ = fromEvent(presetsElement, 'click').pipe(pluck('target'))
const element$ = merge(
  target$.pipe(
    filter(({ tagName }) => tagName === 'P'),
    pluck('parentElement')
  ),
  target$.pipe(filter(({ tagName }) => tagName === 'LI'))
).pipe()

const form = document.querySelector('#shipping')
element$.subscribe(element => {
  const last = document.querySelector('.presets > .active')
  if (last) {
    last.classList.remove('active')
  }
  element.classList.add('active')

  const data = element.dataset
  form.contact_name.value = data.name
  form.address.value = data.address
  form.phone.value = data.phone
})

fromEvent(document.querySelectorAll('#shipping'), 'input')
  .pipe(
    pluck('target', 'form'),
    map(form => ({
      name: form.contact_name.value,
      address: form.address.value,
      phone: form.phone.value,
    }))
  )
  .subscribe(data => {
    const last = document.querySelector('.presets > .active')
    if (last) {
      last.classList.remove('active')
    }

    for (const li of presetsElement.children) {
      const { dataset } = li
      if (
        data.name === dataset.name &&
        data.address === dataset.address &&
        data.phone === dataset.phone
      ) {
        li.classList.add('active')
        break
      }
    }
  })
