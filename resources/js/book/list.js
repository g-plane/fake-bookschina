import Swal from 'sweetalert2'
import addToCart from './toCart'
import { addToFav, removeFromFav } from './toFav'

document.querySelector('ul.books-list').addEventListener('click', async event => {
  const id = event.target.getAttribute('data-book-id')
  if (!id) {
    return
  }

  if (event.target.classList.contains('btn-add-to-cart')) {
    return addToCart(id)
  }

  if (event.target.classList.contains('btn-fav')) {
    if (event.target.getAttribute('data-fav')) {
      if (await removeFromFav(id)) {
        event.target.textContent = '收藏'
        event.target.removeAttribute('data-fav')
      }
      return
    }

    if (await addToFav(id)) {
      Swal.fire({ type: 'success', text: '已加入收藏' })
      event.target.textContent = '取消收藏'
      event.target.setAttribute('data-fav', 'true')
    }
  }
})
