import Swal from 'sweetalert2'
import { post } from '../common/fetch'

export async function addToFav(id) {
  const { code, message } = await post('/user/favorites/add', { book: +id })
  if (code === 0) {
    Swal.fire({ type: 'success', text: '已加入收藏' })
  } else {
    Swal.fire({ type: 'warning', text: message })
  }

  return code === 0
}

export async function removeFromFav(id) {
  const { code, message } = await post('/user/favorites/remove', { book: +id })
  if (code === 0) {
    Swal.fire({ type: 'success', text: '已从收藏中移除' })
  } else {
    Swal.fire({ type: 'warning', text: message })
  }

  return code === 0
}
