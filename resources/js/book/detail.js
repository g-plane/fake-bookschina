import 'element-ui/lib/icon'
import 'element-ui/lib/theme-chalk/icon.css'
import addToCart from './toCart'
import { addToFav, removeFromFav } from './toFav'

const tab = document.querySelector('.book-tab')
const tabElements = Array.from(tab.querySelectorAll('div'))
const detailArea = document.querySelector('.detail-content')
const commentsArea = document.querySelector('.comments')

tab.addEventListener('click', () => {
  detailArea.classList.toggle('hidden')
  commentsArea.classList.toggle('hidden')
  tabElements.forEach(el => el.classList.toggle('active'))
})

const btnToCart = document.querySelector('#to-cart')
btnToCart.addEventListener('click', () => {
  const matches = /(\d+)/.exec(location.pathname)
  if (matches) {
    addToCart(matches[1])
  }
})

const btnToFav = document.querySelector('#to-fav')
btnToFav.addEventListener('click', async () => {
  const matches = /(\d+)/.exec(location.pathname)
  if (!matches) {
    return
  }
  const id = matches[1]

  const favored = btnToFav.getAttribute('data-fav')
  if (favored) {
    if (await removeFromFav(id)) {
      btnToFav.textContent = '收藏'
      btnToFav.removeAttribute('data-fav')
    }
  } else {
    if (await addToFav(id)) {
      btnToFav.textContent = '取消收藏'
      btnToFav.setAttribute('data-fav', 'true')
    }
  }
})

const youMayLike = document.querySelector('.you-may-like')
youMayLike.addEventListener('click', event => {
  const id = event.target.getAttribute('data-book-id')
  if (!id) {
    return
  }

  return addToCart(id)
})
