import Swal from 'sweetalert2'
import { post } from '../common/fetch'

export default async function (id) {
  const { code } = await post('/shoppingCart/add', { book: +id })
  if (code === 0) {
    Swal.fire({ type: 'success', text: '已放入购物车' })
  }

  return code === 0
}
