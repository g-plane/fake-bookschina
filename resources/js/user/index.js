for (const element of document.querySelectorAll('.user-nav > li')) {
  if (location.pathname.startsWith(element.firstChild.getAttribute('href'))) {
    element.classList.add('active')
    break
  }
}
