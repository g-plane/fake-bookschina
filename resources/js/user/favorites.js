import Vue from 'vue'
import Favorites from './Favorites.vue'

new Vue({
  el: 'main',
  render: h => h(Favorites),
})
