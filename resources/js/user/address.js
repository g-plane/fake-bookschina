import Vue from 'vue'
import Address from './Address.vue'

new Vue({
  el: 'main',
  render: h => h(Address)
})
