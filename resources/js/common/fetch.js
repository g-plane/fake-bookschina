import Swal from 'sweetalert2'

export function retrieveToken() {
  const el = document.querySelector('meta[name="csrf-token"]')
  if (el) {
    return el.content
  }

  return ''
}

async function lowLevelFetch(url, method, body) {
  const response = await fetch(url, {
    method: method.toUpperCase(),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': retrieveToken(),
    },
    redirect: 'follow',
    credentials: 'same-origin',
    body: JSON.stringify(body),
  })

  if (response.status === 200) {
    return response.json()
  } else {
    if (response.status === 401) {
      return { code: 1, message: '请先登录' }
    }
    if (response.status === 422) {
      const { errors } = await response.json()
      const firstKey = Object.keys(errors)[0]
      return { code: 1, message: errors[firstKey][0] }
    }

    if (response.headers.get('Content-Type') === 'application/json') {
      Swal.fire({ type: 'error', text: (await response.json()).message })
      return {}
    }
  }
}

export async function get(url) {
  return lowLevelFetch(url, 'GET')
}

export async function post(url, body = {}) {
  return lowLevelFetch(url, 'POST', body)
}
