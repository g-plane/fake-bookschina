# fake-bookschina

## 搭建

### PHP 环境准备

可以使用 phpStudy 或 XAMPP 等软件，需要确保 PHP 版本大于 **7.2.0**。

接着在 phpStudy（等类似软件）中修改网站根目录为项目下的 `public` 目录，
例如原网站根目录为 `D:\phpStudy\WWW`，则改为 `D:\phpStudy\WWW\public`。

同时需要确保 `php` 所在目录位于系统的 `PATH` 环境变量下。

另外，以下 PHP 扩展必须开启：

- Mbstring
- OpenSSL
- PDO (pdo_mysql)
- Tokenizer
- XML
- fileinfo

### 安装依赖

先点击 [这里](https://getcomposer.org/Composer-Setup.exe) 下载 Composer，安装依赖需要这个工具。

下载好之后按照安装向导一步一步安装就行。

接下来，在命令行里，进入项目所在目录，执行以下命令：

```
composer install
```

如果遇到失败，试试下面这个命令：

```
composer install --prefer-dist
```

### 准备配置文件

将项目目录下的 `.env.example` 另存为 `.env` 文件即可，接着运行以下命令：

```
php artisan key:generate
```

### 构建前端

先在 [这里](https://npm.taobao.org/mirrors/node/v10.9.0/node-v10.9.0-x64.msi) 下载 Node.js 然后在 [这里](https://npm.taobao.org/mirrors/yarn/v1.17.3/yarn-1.17.3.msi) 下载 Yarn，编译 CSS 和 JS 会用到。同样是按照向导安装好。

先在项目目录执行以下命令，这个是安装依赖，通常只需要安装一次：

```
yarn
```

CSS 和 JS 文件需要经过编译才能在页面上产生效果，执行以下命令即可进行编译：

```
npm run dev
```

对 CSS 或 JS 文件进行修改后，需要重新编译才能使新代码生效。

另外，还可以运行以下命令，让编译进程保持运行。这样在每次修改 CSS 或 JS 后就不需要手动运行 `npm run dev`，而只需要刷新页面即可：

```
npm run watch
```

## 目录结构说明

`app` 目录为网站核心代码所在目录，网站采用 MVC 模式，因此，`app/Http/Controllers` 下为 Controller 即控制器的代码；
`app/Models` 下为 Model 即模型的代码。

`resources` 为前端相关的目录。其中，`js` 目录存放 JavaScript 代码，`stylus` 存放 CSS 代码，`views` 存放 HTML 代码（其实也是 MVC 中的 View 即视图层）。需要注意的是，不能以 `.html` 结尾，而应该以 `.blade.php` 结尾。

`routes` 为路由相关的目录，通常只用到 `routes/web.php` 文件。

`bootstrap`、`config`、`database`、`public`、`storage`、`tests` 目录目前均可忽略。
